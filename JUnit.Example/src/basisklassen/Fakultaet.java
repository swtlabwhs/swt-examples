package basisklassen;

/**
 * Fakultaetsfunktion
 * (vielleicht eher kuenstlich als Objektmethode
 * und nicht als Klassenmethode realisiert)
 *
 */
public class Fakultaet
{
//	// zunaechst eine triviale unvollstaendige Implementierung
//	public int fakultaet(int zahl)
//    {
//        return 1;
//    }

    public int fakultaet(int zahl)
    {
       assert(zahl>=0); // hier als Vorbedingung (Precondition)
       
       int ergebnis=1;
       while (zahl>1)
       {
          ergebnis = ergebnis*zahl;
          zahl = zahl-1;
       }
       return ergebnis;
    }

    public static void main(String[] args)
    {
        Fakultaet obj=new Fakultaet();
        
        for (int i=0; i<=50; i++) // Achtung: Bereichsueberschreitung ab i=13!
            System.out.println("fak("+i+")="+obj.fakultaet(i));
    }
}
