package basisklassen;

import java.lang.Math;

// Uebungsaufgabe 15
public class QuadratWurzel
{
    public static final double epsilon=0.001;   
    
    public static double quadratWurzel(double zahl)
    {
        assert zahl>=0.0;                   // Vorbedingung fuer Quadratwurzelbestimmung
        
        double ergebnis=2.0;                // geschaetztes (Zwischen-)Ergebnis
        double diff=ergebnis*ergebnis-zahl; // Ungenauigkeit der aktuellen Schaetzung
        
        while (Math.abs(diff)>epsilon)      // noch zu ungenau
        {
            ergebnis=ergebnis-diff/(2.0*ergebnis);
            diff=ergebnis*ergebnis-zahl;
            System.out.println("bisheriges Zwischenergebnis="+ergebnis);
        }
        
        return ergebnis;
    }

    
    public static void protokolliere(double zahl)
    {
        System.out.println("Quadratwurzel von "+zahl);
        System.out.println("Ergebnis="+quadratWurzel(zahl)+"\n");
    }
    
    public static void main(String[] args)
    {
        protokolliere(4);
        protokolliere(2);
        protokolliere(1);
        protokolliere(0.5);
        protokolliere(10000);
    }

}
