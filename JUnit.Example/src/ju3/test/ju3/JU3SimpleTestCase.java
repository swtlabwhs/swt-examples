package test.ju3;

import junit.framework.*;
import basisklassen.Fakultaet;

public class JU3SimpleTestCase extends TestCase
{
	public JU3SimpleTestCase(String name) { super(name); }
	
	// return single TestCase object for TestRunner
	public static Test suite()
	{
		return new JU3SimpleTestCase("runTest");
	}
	
	public void runTest() throws Exception
	{
		Fakultaet obj = new Fakultaet();
		assertTrue(obj.fakultaet(5)==120);
	}
}
