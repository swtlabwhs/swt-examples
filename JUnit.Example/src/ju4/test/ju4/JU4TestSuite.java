package test.ju4;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({SimpleTest.class, TestNormal.class, TestUnnormal.class})
public class JU4TestSuite {}
