package test.ju4;

import org.junit.Test;          // import der @Test-Annotation
import org.junit.Assert;        // import der Assert-Hilfsklasse

import basisklassen.Fakultaet;

public class SimpleTest
{
    
    @Test
    public void fuenf()
    {
        Fakultaet obj = new Fakultaet();
        Assert.assertTrue(obj.fakultaet(5)==120);
    }
}
