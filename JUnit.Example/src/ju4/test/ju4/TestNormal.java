package test.ju4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import basisklassen.Fakultaet;

public class TestNormal
{
    Fakultaet obj;
    
    @Before             // Setup fuer alle TestCases
    public void setUp()
    {
        obj = new Fakultaet();
    }
   
    
    @Test
    public void fuenf()
    {
        Assert.assertTrue(obj.fakultaet(5)==120);
    }

    @Test
    public void randNull()
    {
        Assert.assertTrue(obj.fakultaet(0)==1);
    }

    @Test
    public void randEins()
    {
        Assert.assertTrue(obj.fakultaet(1)==1);
    }
}
