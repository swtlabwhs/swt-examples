package test.ju4;

import org.junit.Before;
import org.junit.Test;

import basisklassen.Fakultaet;

public class TestUnnormal
{
    Fakultaet obj;
    
    @Before
    public void setUp()
    {
        obj = new Fakultaet();
    }
   
    
    @Test(expected=Error.class)
    public void minusFuenf()
    {
        obj.fakultaet(-5);
    }

    @Test(expected=Error.class)
    public void randMinusEins()
    {
        obj.fakultaet(-1);
    }

    @Test(expected=Exception.class)
    public void zuGross()
    {
        obj.fakultaet(10000);
    }
}
