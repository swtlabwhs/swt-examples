package test;

import org.junit.runner.RunWith;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.runner.JUnitPlatform;

@RunWith(JUnitPlatform.class)
@SelectPackages({"test.standard", "test.boundary", "test.special"})
public class AllTests { }
