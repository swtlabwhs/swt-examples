package test;

import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Assertions;

import java.time.Duration;

import basisklassen.Fakultaet;

public class PerformanceTest
{
    
//	@RepeatedTest(5)
    @Test
    public void schnell()
    {
        Fakultaet obj = new Fakultaet();
        Assertions.assertTimeout(Duration.ofMillis(50),
        		() -> {obj.fakultaet(12);} );
    }
    
}