package test.boundary;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.*;	// erlaubt auch kuerzere Schreibweise!

import basisklassen.Fakultaet;

public class BoundardyTest
{
    
    @Test
    public void zero()
    {
        Fakultaet obj = new Fakultaet();
        Assertions.assertTrue(obj.fakultaet(0)==1);
    }
    
    @Test
    public void eins()
    {
        Fakultaet obj = new Fakultaet();
        assertTrue(obj.fakultaet(1)==1);
    }
    
    @Test
    public void minusEins()
    {
        Fakultaet obj = new Fakultaet();
        assertThrows(java.lang.AssertionError.class, 
        		() -> {obj.fakultaet(-1);}  );
    }

}
