package test.special;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import basisklassen.Fakultaet;

public class SpecialTest
{
    
    @Test
    public void negativ()
    {
        Fakultaet obj = new Fakultaet();
        Assertions.assertThrows(java.lang.AssertionError.class, 
            		() -> {obj.fakultaet(-5);}  );
    }
    
    @Test
    public void zuGross()
    {
        Fakultaet obj = new Fakultaet();
        Assertions.assertThrows(java.lang.Error.class, 
        		() -> {obj.fakultaet(20);}  );
    }

}
