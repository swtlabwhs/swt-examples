package test.standard;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import basisklassen.Fakultaet;

public class StandardTest
{
	Fakultaet obj;
	
	@BeforeEach
	public void setup()
	{
		obj = new Fakultaet();
	}
	
    
    @Test
    public void drei()
    {
        Assertions.assertTrue(obj.fakultaet(3)==6);
    }
    
    @Test
    public void fuenf()
    {
        Assertions.assertTrue(obj.fakultaet(5)==120);
    }
    
    @Test
    public void zwoelf()
    {
        Assertions.assertTrue(obj.fakultaet(12)==479001600);
    }

}
